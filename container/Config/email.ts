export const transportOptions = {
  service: 'gmail',
  auth: {
    user: process.env.EMAIL_NODEMAILER,
    pass: process.env.PASSWORD_NODEMAILER,
  },
  tls: {
    rejectUnauthorized: false,
  },
};
