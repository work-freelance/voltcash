import Express from "express";
import { Container } from "typedi";
import { useContainer, useExpressServer } from "routing-controllers";
import Morgan from "morgan";
import BodyParser from "body-parser";
import AuthController from "../Controllers/auth/auth-controller";
import cors from 'cors';
import CORS_CONFIG from "./cors";
import AdministratorController from "../Controllers/administrator/administrator-controller";


const server = Express();
/**
 * Middlewares
 */
server.use(BodyParser.json());
server.use(BodyParser.urlencoded({ extended: false }));
server.use(Morgan("dev"));
server.use(cors(CORS_CONFIG));
/***
 * Injection of services in routes
 */
useContainer(Container);
/***
 * Load Routes
 */
useExpressServer(server, {
  routePrefix: "/api",
  controllers: [AuthController, AdministratorController],
});

export default server;
