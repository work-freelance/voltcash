import { createConnection } from "typeorm";
/****
 * Conexion de base de datos
 */
export const ConectionPostgreSQL = createConnection();