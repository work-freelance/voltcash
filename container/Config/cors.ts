const CORS_CONFIG = {
   origin: 'http://localhost:3000',
   optionsSuccessStatus: 200
};

export default CORS_CONFIG;
