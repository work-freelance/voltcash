import { Inject } from 'typedi';
import { JsonController, Res, Req, Post, Get, UseBefore } from 'routing-controllers';
import { Request, Response } from 'express';
import AdministratorService from '../../Services/administrator/administrator-service';
import UserInterface from '../../Database/Interfaces/user-interface';
import messageHandler from '../../Handlers/message-handler';
import { verifyToken } from '../../Middlewares/auth-token-middleware';

@JsonController('/administrator')
@UseBefore(verifyToken)
export default class AdministratorController {
  @Inject()
  private administratorService: AdministratorService;

  @Post('/createUser')
  async createUser(@Req() request: Request, @Res() response: Response) {
    const data = await this.administratorService.createUser(
      request.body as UserInterface
    );
    return messageHandler(data, 200, response);
  }

  @Get('/getUsers')
  async viewUsers(@Res() response: Response) {
    const data = await this.administratorService.viewUsers();
    return messageHandler(data, 200, response);
  }
}
