import { Inject } from 'typedi';
import { JsonController, Res, Req, Post } from 'routing-controllers';
import { Request, Response } from 'express';
import AuthService from '../../Services/auth/auth-service';
import messageHandler from '../../Handlers/message-handler';

@JsonController('/auth')
export default class AuthController {
  @Inject()
  private authService: AuthService;

  @Post('/signIn')
  async signIn(@Req() request: Request, @Res() response: Response) {
    const data = await this.authService.signIn(request.body);
    return messageHandler(data, 200, response);
  }

  @Post('/recoveryPassword')
  async recoveryPassword(@Req() request: Request, @Res() response: Response) {
    const data = await this.authService.recoveryPassword(request.body);
    return messageHandler(data, 200, response);
  }
}
