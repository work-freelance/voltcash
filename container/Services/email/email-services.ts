import { createTransport } from 'nodemailer';
import { Service } from 'typedi';
import { transportOptions } from '../../Config/email';

@Service()
export default class EmailService {
  /**
   * @method */
  sendEmail(configOptionEmail) {
    createTransport({ ...transportOptions }).sendMail(
      configOptionEmail,
      (error, response) => (error ? console.log(error) : response),
    );
  }
}
