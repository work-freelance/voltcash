import { Service } from 'typedi';
import UserInterface from '../../Database/Interfaces/user-interface';
import User from '../../Database/Entities/user';
import {
  createUserQuery,
  ViewRoleQuery,
  ViewUsersQuery,
} from '../../Database/Queries/user-query';
import Role from '../../Database/Entities/role';

@Service()
export default class AdministratorService {
  /**
   * @method Create user
   * @param user
   */

  async createUser(user: UserInterface): Promise<string> {
    try {
      const queryRole = await ViewRoleQuery(Role, { idRole: user.role });
      if (queryRole !== undefined) {
        return createUserQuery(User, user)
          .then((_) => {
            return Promise.resolve('User created');
          })
          .catch((_) => {
            return Promise.resolve('This email already exists in the database');
          });
      }
    } catch (e) {
      console.log(e);
      return Promise.reject('Error in database');
    }
  }
  /**
   * List to users
   * @method
   */
  async viewUsers(): Promise<Array<User> | string> {
    try {
      const query = await ViewUsersQuery(User);
      if (query.length !== 0) {
        return Promise.resolve(query as Array<User>);
      } else {
        return Promise.resolve('No users');
      }
    } catch (_) {
      return Promise.reject('Error in database');
    }
  }
}
