import { Inject, Service } from 'typedi';
import UserInterface from '../../Database/Interfaces/user-interface';
import User from '../../Database/Entities/user';
import JWTService from '../jwt/jwt-service';
import {
  signQuery,
  recoveryPasswordQuery,
} from '../../Database/Queries/auth-query';
import EmailService from '../email/email-services';
import { emailHTML } from '../../Handlers/email-handler';

@Service()
export default class AuthService {
  @Inject()
  private emailService: EmailService;

  @Inject()
  private jwtService: JWTService;

  async signIn(user: UserInterface): Promise<object | string> {
    const { emailUser, passwordUser } = user;
    if (emailUser && passwordUser) {
      try {
        const query = await signQuery(User, { emailUser, passwordUser });
        if (query !== undefined && query.length !== 0) {
          const token = this.jwtService.signToken({
            emailUser,
            passwordUser,
          });
          const data = query as any;
          data[0].token = token;
          return Promise.resolve(data[0]);
        } else {
          return Promise.reject('User no found');
        }
      } catch (error) {
        console.log(error);
        return Promise.reject('Error in database');
      }
    }
  }

  async recoveryPassword(user: UserInterface): Promise<any | string> {
    const { emailUser } = user;
    try {
      const query: any = await recoveryPasswordQuery(User, { emailUser });
      if (query !== undefined) {
        this.emailService.sendEmail({
          from: process.env.EMAIL_NODEMAILER,
          to: emailUser,
          subject: 'Recuperacion de contraseña',
          html: emailHTML(query.nameUser, query.passwordUser),
        });
        return Promise.resolve('Email sent successfully');
      } else {
        return Promise.reject('User no found');
      }
    } catch (_) {
      return Promise.reject('Error in database');
    }
  }
}
