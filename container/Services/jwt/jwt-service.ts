import { sign, verify } from "jsonwebtoken";
import { Service } from "typedi";

@Service()
export default class JWTService {
  
  signToken(body = {}): string {
    return sign(body, process.env.TOKEN_AUTH, {
      expiresIn: "30s",
    });
  }
}
