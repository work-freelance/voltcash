import { Request, Response } from 'express';
import { verify } from 'jsonwebtoken';
import messageHandler from '../Handlers/message-handler';

export const verifyToken = (request: Request, response: Response) => {
  const headers: any = request.headers;
  if (headers.authorization) {
    try {
      verify(headers.authorization, process.env.TOKEN_AUTH);
      messageHandler('successfully validated token', 200, response);
    } catch (_) {
      messageHandler('Token already inspired', 500, response);
    }
  } else {
    messageHandler('there is no validation token', 500, response);
  }
};
