export default interface RoleInterface {
    idRole: number;
    typeRole: string;
}