import  RoleInterface  from "./role-interface";

export default interface UserInterface {
  idUser: number;
  nameUser: string;
  emailUser: string;
  passwordUser: string;
  phoneUser: number;
  role: RoleInterface;
}
