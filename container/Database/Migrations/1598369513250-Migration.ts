import {MigrationInterface, QueryRunner} from "typeorm";

export class Migration1598369513250 implements MigrationInterface {
    name = 'Migration1598369513250'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_2b5eea11ccd09286c9dbec9f916"`);
        await queryRunner.query(`CREATE TABLE "role" ("idRole" SERIAL NOT NULL, "typeRole" character varying(30) NOT NULL, CONSTRAINT "PK_1e9e35d9ab72d2ed91655d6cdf7" PRIMARY KEY ("idRole"))`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_2b5eea11ccd09286c9dbec9f916" FOREIGN KEY ("id_rol") REFERENCES "role"("idRole") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_2b5eea11ccd09286c9dbec9f916"`);
        await queryRunner.query(`DROP TABLE "role"`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_2b5eea11ccd09286c9dbec9f916" FOREIGN KEY ("id_rol") REFERENCES "ROL"("id_rol") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
