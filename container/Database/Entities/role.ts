import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import RoleInterface from '../Interfaces/role-interface';
import User from './user';

@Entity()
export default class Role implements RoleInterface {
  @PrimaryGeneratedColumn()
  idRole: number;

  @Column({ type: 'varchar', length: 30, nullable: false })
  typeRole: string;

  @OneToMany((type) => User, (user) => user.idUser)
  user: Array<User>;
}
