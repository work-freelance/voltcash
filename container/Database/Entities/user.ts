import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import UserInterface from '../Interfaces/user-interface';
import Role from './role';

@Entity()
export default class User implements UserInterface {
  @PrimaryGeneratedColumn()
  idUser: number;

  @Column({
    length: 50,
    type: 'varchar',
    nullable: false,
  })
  nameUser: string;

  @Column({
    length: 50,
    type: 'varchar',
    unique: true,
    nullable: false,
  })
  emailUser: string;

  @Column({
    length: 30,
    type: 'varchar',
    nullable: false,
  })
  passwordUser: string;

  @Column({
    type: 'bigint',
    nullable: false,
  })
  phoneUser: number;

  @ManyToOne((type) => Role, (role) => role.idRole)
  @JoinColumn({ name: 'id_rol' })
  role: Role;
}
