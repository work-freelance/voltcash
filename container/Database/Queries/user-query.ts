import { getRepository } from 'typeorm';

export const createUserQuery = (model, options = {}) => {
  return getRepository(model).save(options);
};

export const ViewUsersQuery = (model) => {
  return getRepository(model)
    .createQueryBuilder('user')
    .innerJoinAndSelect('user.role', 'role')
    .getMany();
};

export const ViewRoleQuery = (model, options) => {
  return getRepository(model)
    .createQueryBuilder('role')
    .where({ ...options })
    .getOne();
};
