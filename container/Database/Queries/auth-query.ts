import { getRepository } from 'typeorm';
import CONSULTS_SELECTS from '../../Handlers/consults-handler';

export const signQuery = (model, options = {}) => {
  return getRepository(model)
    .createQueryBuilder('user')
    .innerJoinAndSelect('user.role', 'role')
    .where(CONSULTS_SELECTS.singInUser, { ...options })
    .getMany();
};

export const recoveryPasswordQuery = (model, options = {}) => {
  return getRepository(model)
    .createQueryBuilder('user')
    .where({ ...options })
    .getOne();
};
