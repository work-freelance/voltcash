import { Response } from "express";

const messageHandler = (
  message: string | object,
  code: number,
  response: Response
) => response.status(code).json(message);

export default messageHandler;