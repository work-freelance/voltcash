const date = new Date();

export const emailHTML = (name: string, password: string): string => {
  return `
        <div style="width: 80%; margin: auto; font-family: arial; color: black;">
            <h1 style="text-align: center">
                Bienvenido a control curriculo
                <span>
                    ${name}
                </span>
            </h1>
            <p style="text-align: center">
                Le recordamos que su contraseña actual es
                <span style="font-weight: bold;" >
                    ${password}
                </span>
            </p>
            <h1 style="font-weight: bold; text-align: center">
                <span>
                    Fecha: ${date.getDay()}/${date.getMonth()}/${date.getFullYear()}-${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}
                </span>
            </h1>
        </div>
    `;
};
