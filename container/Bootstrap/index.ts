import DotEnv from "dotenv";
import { ConectionPostgreSQL } from "../Config/database";
import "reflect-metadata";
import server from "../Config/server";
DotEnv.config();
/***
 * Database Config
 */
ConectionPostgreSQL.then(() =>
  console.log(
    `Successful ${process.env.NAME_DATABASE} database connection 🚀🚀🚀`
  )
).catch((error) => console.log(error));

/***
 * Load Server
 */
server.listen(process.env.PORT_SERVER, () =>
  console.log(`http://localhost:${process.env.PORT_SERVER} 🚀🚀🚀`)
);
